module Main where
import System.Environment
import qualified Data.HashTable as HT
import Control.Monad
import Data.Char
import Data.List

main = do
        args <- getArgs
        input <- case (length args) > 0 of
                        True -> createStringFromTextFiles args 
                        False -> getContents
        printFrequencyGraph input

createStringFromTextFiles :: [String] -> IO String
createStringFromTextFiles files = 
        (return . unwords) =<< mapM readFile files
        
countWords wordslist =
        let countHelper dict word  = do
                value <- HT.lookup dict word
                case value of
                        Just x -> HT.update dict word (x + 1)
                        Nothing -> HT.update dict word 1
                return dict in
        do 
                newdict <- (HT.new (==) HT.hashString)
                ans <- foldM countHelper newdict wordslist
                lst <- HT.toList ans
                return lst

findLongestWord :: [String] -> Int
findLongestWord = maximum . map length

parseWordString = 
        words . map toLower . filter (`notElem` "_[]\"!?.;,()&") .
                map (\ c -> if (c=='-') then ' ' else c)

sortWordList :: [(String,Int)] -> [(String,Int)]
sortWordList = 
        let sortFunc (_,v1) (_,v2) =
                compare v2 v1 in
        sortBy sortFunc

sortedFrequency :: String -> IO [(String,Int)] 
sortedFrequency str = do 
        unsortedList <- countWords $ parseWordString str 
        return $ sortWordList unsortedList

createFrequencyGraph :: [(String,Int)] -> String 
createFrequencyGraph list =
        let printLine (str,freq) =
                str ++ (replicate (maxlen - (length str)) ' ') ++
                  (replicate freq '#') ++ "\n" 
            maxlen = (findLongestWord (map fst list)) + 1 in
        foldl (\acc itm -> acc ++ printLine itm) "" $ divList list maxlen

divList :: [(String,Int)] -> Int -> [(String,Int)]
divList [] _ = []
divList lst@(hd:_) maxlen = 
        let factor = ((fromIntegral (snd hd)) :: Double)/ (fromIntegral (80 - maxlen)) in
        if factor > 1 then
                map (\ (word, freq) -> 
                        (word,ceiling $ (fromIntegral freq) / factor)) lst
        else
                lst

printFrequencyGraph :: String -> IO ()
printFrequencyGraph str = do
        ans <-sortedFrequency str                 
        let output = createFrequencyGraph ans
        putStr output
